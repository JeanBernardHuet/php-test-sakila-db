<?php

/**
 * Full list of films from 'film_list' view
 */
const REQ_FILM_FULL_LIST = "SELECT * FROM film_list";

/**
 * Informations about 1 film
 *
 * @var :ValFilmID integer between 1 and 99999 - MANDATORY - NO DEFAULT
 */
const REQ_FILM_INFO = "SELECT * FROM film WHERE film_id = :ValFilmID";

/**
 * Add a film
 *
 * @var :ValTitle string max length 255 - MANDATORY - NO DEFAULT
 * @var :ValDescription string max length 65535 - OPTIONAL
 * @var :ValReleaseYear year 4 digits - OPTIONAL
 * @var :ValLanguageID integer as from 'language' table, between 1 and 999 - MANDATORY - NO DEFAULT
 */
const REQ_FILM_ADD = "INSERT INTO film (title, description, release_year, language_id) VALUES (:ValTitle, :ValDescription, :ValReleaseYear, :ValLanguageID)";

/**
 * Modify film title
 *
 * @var :ValNewTitle string max length 255 - MANDATORY
 * @var :ValFilmID integer between 1 and 99999 - MANDATORY - NO DEFAULT
 */
const REQ_FILM_MODIF_TITLE = "UPDATE film SET title = :ValNewTitle WHERE film_id = :ValFilmID";

/**
 * Delete a film
 *
 * @var :ValFilmID integer between 1 and 99999 - MANDATORY - NO DEFAULT
 */
const REQ_FILM_DELETE = "DELETE FROM film WHERE film_id = :ValFilmID";

/**
 * Add a film
 *
 * @var :ValTitle string max length 255 - MANDATORY - NO DEFAULT
 * @var :ValDescription string max length 65535 - OPTIONAL
 * @var :ValReleaseYear year 4 digits - OPTIONAL
 * @var :ValLanguageID integer as from 'language' table, between 1 and 999 - MANDATORY - NO DEFAULT
 */
// const REQ_FILM_ADD_COMPLETE = "INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features) VALUES (:ValTitle, :ValDescription, :ValReleaseYear, :ValLanguageID, :ValOriginalLanguageID, :ValRentalDuration, :ValRentalRate, :ValLength, :ValReplacementCost, :ValRating, :ValSpecialFeatures)";
const REQ_FILM_ADD_COMPLETE = "INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features) VALUES (:title, :description, :release_year, :language_id, :original_language_id, :rental_duration, :rental_rate, :length, :replacement_cost, :rating, :special_features)";
