<?php
declare(strict_types = 1);

require_once "DbServerParams.php";
require_once "SakilaFilmsRequests.php";
require_once "SakilaCustomersRequests.php";

function RequestManager(object $RequestToHandle = null, string $InfoMessage = "", string $NoDataMessage = "", array $RequestParams = [], $IsSelectRequest = true)
{
    if (is_a($RequestToHandle, "PDOStatement")) {
        try {
            (boolean) $ReqSuccess = $RequestToHandle->execute($RequestParams);
            if ($ReqSuccess) {
                print PHP_EOL . $InfoMessage . PHP_EOL . PHP_EOL;
                if ($IsSelectRequest) {
                    while ($DataLine = $RequestToHandle->fetch(PDO::FETCH_ASSOC)) {
                        print_r($DataLine);
                    }
                }
            } else {
                print $NoDataMessage . PHP_EOL . PHP_EOL;
            }
        } catch (Exception $ExceptionRaised) {
            print "Une erreur inattendue est survenue !" . PHP_EOL;
            print ("Erreur : " . $ExceptionRaised->getMessage()) . PHP_EOL;
            print "Informations sur l'état de la requète :" . PHP_EOL;
            print_r($RequestToHandle->errorInfo());
        }
    } else {
        print "ERREUR !!! La variable passée en 1er argument de RequestManager() n'est pas une requète de type PDOStatement" . PHP_EOL . PHP_EOL;
    }
}

(object) $MyDB = null;
(boolean) $PromptPwd = false;

try {
    $MyDB = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD);
} catch (Exception $ExceptionRaised) {
    print "La connexion n'a pas pu se faire avec les identifiants par défaut." . PHP_EOL . PHP_EOL;
    $PromptPwd = true;
}
if ($PromptPwd) {
    // Demander l'identifiant et le mot de passe
    print("Saisissez le nom du compte pour vous connecter au serveur MySQL : ");
    (string) $DbServerLogin = trim(fgets(STDIN));
    print("Saisissez le mot de passe : ");
    (string) $DbServerPassword = trim(fgets(STDIN));
    // connexion à la bdd
    try {
        $MyDB = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, $DbServerLogin, $DbServerPassword);
    } catch (Exception $ExceptionRaised) {
        print "La connexion au serveur MySQL est impossible. Voir le détail ci-dessous." . PHP_EOL;
        print_r($MyDB->errorInfo());
        die("Erreur : " . $ExceptionRaised->getMessage());
    }
}
print PHP_EOL . "La connexion au serveur de bases de données est désormais active." . PHP_EOL . PHP_EOL;
$MyDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// Préparation des requètes utilisées souvent
print PHP_EOL . "Préparation des requètes." . PHP_EOL . PHP_EOL;
$MyReqCustomersOverdueRental = $MyDB->prepare(REQ_CUSTOMERS_WITH_OVERDUE_RENTALS);
$MyReqFullFilmList = $MyDB->prepare(REQ_FILM_FULL_LIST);
$MyReqFilmInfo = $MyDB->prepare(REQ_FILM_INFO);
$MyReqFilmAdd = $MyDB->prepare(REQ_FILM_ADD);
$MyReqFilModifyName = $MyDB->prepare(REQ_FILM_MODIF_TITLE);
$MyReqFilmDelete = $MyDB->prepare(REQ_FILM_DELETE);

(string) $Answer = "";
while ("0" != $Answer) {
    print "Que voulez-vous faire ?" . PHP_EOL;
    print "1 > Afficher la liste complète des films" . PHP_EOL;
    print "2 > Afficher la liste complète des clients en retard dans le rendu de leur DVD" . PHP_EOL;
    print "3 > Afficher les informations pour 1 film en particulier" . PHP_EOL;
    print "4 > Ajouter un film" . PHP_EOL;
    print "5 > Modifier le nom d'un film en particulier" . PHP_EOL;
    print "6 > Supprimer un film en particulier" . PHP_EOL;
    print "7 > Exécuter une transaction" . PHP_EOL;
    print "0 > Arrêter le programme" . PHP_EOL;
    print "Votre choix : ";
    $Answer = trim(fgets(STDIN));
    switch ($Answer) {
        case "1":
            RequestManager($MyReqFullFilmList, "La liste complète des films :", "Il n'y a aucun film.");
            break;
        case "2":
            RequestManager($MyReqCustomersOverdueRental, "La liste complète des clients en retard dans le rendu de leur DVD :", "Il n'y a aucun client en retard.");
            break;
        case "3":
            print "Saisissez l'ID du film dont vous voulez connaitre les détails : ";
            (int) $FilmID = intval(trim(fgets(STDIN)));
            $MyReqParams = [
                ":ValFilmID" => $FilmID
            ];
            RequestManager($MyReqFilmInfo, "Les informations sur le film d'ID " . $FilmID . ' :', "Il n'y a aucune information disponible.", $MyReqParams);
            break;
        case "4":
            print "OBLIGATOIRE - Saisissez le nom du film à ajouter (255 caractères maximum) : ";
            (string) $FilmTitle = trim(fgets(STDIN));
            print "Optionnel - Saisissez une description pour le film à ajouter (65535 caractères maximum) : ";
            (string) $FilmDescription = trim(fgets(STDIN));
            print "Optionnel - Saisissez l'année de sortie du film à ajouter au format AAAA : ";
            (string) $FilmReleaseYear = trim(fgets(STDIN));
            print "OBLIGATOIRE - Saisissez le code de langue du film à ajouter : ";
            (int) $FilmLanguageID = intval(trim(fgets(STDIN)));
            $MyReqParams = [
                ":ValTitle" => $FilmTitle,
                ":ValDescription" => $FilmDescription,
                ":ValReleaseYear" => $FilmReleaseYear,
                ":ValLanguageID" => $FilmLanguageID
            ];
            RequestManager($MyReqFilmAdd, "Le film qui a pour titre " . $FilmTitle . " sorti en " . $FilmReleaseYear . " a été ajouté.", "Il n'y a pas de film à ajouter.", $MyReqParams, false);
            break;
        case "5":
            print "Saisissez l'ID du film dont vous voulez modifier le nom : ";
            (int) $FilmID = intval(trim(fgets(STDIN)));
            print "Saisissez le nouveau nom du film : ";
            (string) $FilmTitle = trim(fgets(STDIN));
            $MyReqParams = [
                ":ValFilmID" => $FilmID,
                ":ValNewTitle" => $FilmTitle
            ];
            RequestManager($MyReqFilModifyName, "Le nom du film d'ID " . $FilmID . " a bien été modifié.", "Il n'y a aucun nom à modifier.", $MyReqParams, false);
            break;
        case "6":
            print "Saisissez l'ID du film que vous voulez supprimer : ";
            (int) $FilmID = intval(trim(fgets(STDIN)));
            $MyReqParams = [
                ":ValFilmID" => $FilmID
            ];
            RequestManager($MyReqFilmDelete, "Le film d'ID " . $FilmID . " a bien été supprimé.", "Il n'y a aucun film à supprimer.", $MyReqParams, false);
            break;
        case '7':
            if (! ($MyDB->inTransaction())) {
                try {
                    $MyDB->beginTransaction();
                    print PHP_EOL . "Cette transaction va faire 3 choses :" . PHP_EOL;
                    print "- copier les informations sur le film dont vous allez fournir l'ID ;" . PHP_EOL;
                    print "- supprimer la ligne du film du film sélectionné" . PHP_EOL;
                    print "- créer une nouvelle ligne avec les informations copiées" . PHP_EOL;
                    print "La nouvelle ligne aura un nouvel ID." . PHP_EOL;
                    print PHP_EOL . "À chaque étape, vérifiez ce qui se passe dans la base de donnée (avec DBeaver par exemple)." . PHP_EOL;

                    // Demander l'ID du film à traiter
                    print PHP_EOL . "Saisissez l'ID du film dont vous voulez modifier l'ID : ";
                    (int) $FilmID = intval(trim(fgets(STDIN)));
                    $MyReqParams = [
                        ":ValFilmID" => $FilmID
                    ];
                    // Copier les données d'un film
                    (boolean) $ReqResult = $MyReqFilmInfo->execute($MyReqParams);
                    if ($ReqResult) {
                        (array) $DataLine = $MyReqFilmInfo->fetch(PDO::FETCH_ASSOC);

                        // Le supprimer
                        $MyReqFilmDelete->execute([
                            ":ValFilmID" => $DataLine["film_id"]
                        ]);
                        if (1 == $MyReqFilmDelete->rowCount()) {
                            print PHP_EOL . "Le film d'ID {$DataLine["film_id"]} a bien été supprimé." . PHP_EOL . PHP_EOL;
                        }

                        $MyRequest = $MyDB->prepare(REQ_FILM_ADD_COMPLETE);
                        (array) $MyParams = [];

                        $DataLineElem = boolval(count($DataLine));
                        while ($DataLineElem !== false) {
                            if (("film_id" != key($DataLine)) && ("last_update" != key($DataLine))) {
                                $MyParams[":" . key($DataLine)] = $DataLine[key($DataLine)];
                            }
                            $DataLineElem = next($DataLine);
                        }

                        // if (isset($DataLine["title"])) {
                        // $MyParams[":ValTitle"] = $DataLine["title"];
                        // } else {
                        // $MyParams[":ValTitle"] = "Un film sans titre";
                        // }
                        // if (isset($DataLine["description"])) {
                        // $MyParams[":ValDescription"] = $DataLine["description"];
                        // }
                        // if (isset($DataLine["release_year"])) {
                        // $MyParams[":ValReleaseYear"] = $DataLine["release_year"];
                        // }
                        // if (isset($DataLine["language_id"])) {
                        // $MyParams[":ValLanguageID"] = $DataLine["language_id"];
                        // } else {
                        // $MyParams[":ValLanguageID"] = 1;
                        // }
                        // if (isset($DataLine["original_language_id"])) {
                        // $MyParams[":ValOriginalLanguageID"] = $DataLine["original_language_id"];
                        // }
                        // if (isset($DataLine["rental_duration"])) {
                        // $MyParams[":ValRentalDuration"] = $DataLine["rental_duration"];
                        // }
                        // if (isset($DataLine["rental_rate"])) {
                        // $MyParams[":ValRentalRate"] = $DataLine["rental_rate"];
                        // }
                        // if (isset($DataLine["length"])) {
                        // $MyParams[":ValLength"] = $DataLine["length"];
                        // }
                        // if (isset($DataLine["replacement_cost"])) {
                        // $MyParams[":ValReplacementCost"] = $DataLine["replacement_cost"];
                        // }
                        // if (isset($DataLine["rating"])) {
                        // $MyParams[":ValRating"] = $DataLine["rating"];
                        // }
                        // if (isset($DataLine["special_features"])) {
                        // $MyParams[":ValSpecialFeatures"] = $DataLine["special_features"];
                        // }

                        // L'ajouter à nouveau avec un nouvel ID
                        $MyRequest->execute($MyParams);
                        if (1 == $MyReqFilmDelete->rowCount()) {
                            print PHP_EOL . "Le film {$DataLine["title"]} a bien été ajouté." . PHP_EOL . PHP_EOL;
                        }

                        // Demander la confirmation de l'échange d'ID
                        $Answer = "";
                        while (("O" != $Answer) && ("N" != $Answer)) {
                            print PHP_EOL . "Confirmez-vous l'échange d'ID ? (O/N) ";
                            $Answer = strtoupper(trim(fgets(STDIN)));
                            switch ($Answer) {
                                case "O":
                                    print PHP_EOL . "Confirmation du changement d'ID." . PHP_EOL . PHP_EOL;
                                    $MyDB->commit();
                                    break;
                                case "N":
                                    print PHP_EOL . "Annulation du changement d'ID." . PHP_EOL . PHP_EOL;
                                    $MyDB->rollBack();
                                    break;
                                default:
                                    print PHP_EOL . "Je n'ai pas compris votre réponse." . PHP_EOL . PHP_EOL;
                                    break;
                            }
                        }
                    }
                } catch (Exception $ExceptionRaised) {
                    $MyDB->rollBack();
                    print_r($MyDB->errorInfo());
                    print_r($MyRequest->errorInfo());
                    die("Error: " . $ExceptionRaised->getMessage());
                }
            } else {
                print 'Impossible de commencer la transaction. Une autre transaction est déjà en cours.' . PHP_EOL . PHP_EOL;
            }
            break;
        case "0":
            print PHP_EOL . "Au revoir." . PHP_EOL;
            break;
        default:
            print PHP_EOL . "Je ne comprends votre réponse, merci de saisir à nouveau votre choix." . PHP_EOL . PHP_EOL;
            break;
    }
}
