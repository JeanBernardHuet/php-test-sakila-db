<?php

// To connect to local MySQL server with no password for root
const DB_DRIVER = 'mysql';
const DB_HOST = 'localhost';
const DB_NAME = 'sakila';
const DEFAULT_DB_LOGIN = 'root';
const DEFAULT_DB_PWD = '';
